<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderingRepository")
 */
class Ordering
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", name="date_creation")
     */
    private $date_creation;

    /**
     * @ORM\Column(type="datetime", name="date_charged", nullable=true)
     */
    private $date_charged;

    /**
     * @ORM\Column(type="string", name="livraison", nullable=true)
     */
    private $livraison;

    /**
     * @ORM\Column(type="string", name="facturation", nullable=true)
     */
    private $facturation;

    /**
     * @ORM\Column(type="string", name="status", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="float", name="total_ht", nullable=true)
     */
    private $total_ht;

    /**
     * @ORM\Column(type="float", name="total_tva", nullable=true)
     */
    private $total_tva;

    /**
     * @ORM\Column(type="float", name="total_ttc", nullable=true)
     */
    private $total_ttc;

    /**
     * Many Ordering have One User.
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orderings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Paniers have Many Products.
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", cascade={"persist"})
     * @ORM\JoinTable(name="orderings_products",
     *      joinColumns={@ORM\JoinColumn(name="ordering_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private $products;

    /**
     * Ordering constructor.
     */
    public function __construct()
    {
        $this->date_creation = new \DateTime('now');
        $this->products = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return mixed
     */
    public function getDateCharged()
    {
        return $this->date_charged;
    }

    /**
     * @param mixed $date_charged
     */
    public function setDateCharged($date_charged): void
    {
        $this->date_charged = $date_charged;
    }

    /**
     * @return mixed
     */
    public function getLivraison()
    {
        return $this->livraison;
    }

    /**
     * @param mixed $livraison
     */
    public function setLivraison($livraison)
    {
        $this->livraison = $livraison;
    }

    /**
     * @return mixed
     */
    public function getFacturation()
    {
        return $this->facturation;
    }

    /**
     * @param mixed $facturation
     */
    public function setFacturation($facturation)
    {
        $this->facturation = $facturation;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTotalHt()
    {
        return $this->total_ht;
    }

    /**
     * @param mixed $total_ht
     */
    public function setTotalHt($total_ht)
    {
        $this->total_ht = $total_ht;
    }

    /**
     * @return mixed
     */
    public function getTotalTva()
    {
        return $this->total_tva;
    }

    /**
     * @param mixed $total_tva
     */
    public function setTotalTva($total_tva)
    {
        $this->total_tva = $total_tva;
    }

    /**
     * @return mixed
     */
    public function getTotalTtc()
    {
        return $this->total_ttc;
    }

    /**
     * @param mixed $total_ttc
     */
    public function setTotalTtc($total_ttc)
    {
        $this->total_ttc = $total_ttc;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

}
