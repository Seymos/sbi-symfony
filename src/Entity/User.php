<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="email")
     */
    private $email;

    /**
     * @ORM\Column(type="string", name="username")
     */
    private $username;

    /**
     * @ORM\Column(type="string", name="password")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", name="active")
     */
    private $active;

    /**
     * @ORM\Column(type="array", name="roles")
     */
    private $roles;

    /**
     * @ORM\Column(type="string", name="token")
     */
    private $token;

    /**
     * One User has Many Addresses.
     * @ORM\Column(type="array", name="addresses")
     * @ORM\OneToMany(targetEntity="App\Entity\UserAddress", mappedBy="user")
     */
    private $addresses;

    /**
     * One User has One Panier.
     * @ORM\OneToOne(targetEntity="App\Entity\Panier", mappedBy="user")
     */
    private $panier;

    /**
     * One User has Many Orderings.
     * @ORM\OneToMany(targetEntity="App\Entity\Ordering", mappedBy="user")
     */
    private $orderings;


    public function __construct()
    {
        $this->active = false;
        $this->roles = ['ROLE_USER'];
        $this->addresses = new ArrayCollection();
        $this->orderings = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    public function setToken()
    {
        $this->token = md5(uniqid("token_", true));
    }

    public function resetToken()
    {
        $this->token = '';
    }

    /**
     * @return mixed
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param mixed $addresses
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
    }

    /**
     * @param UserAddress $address
     */
    public function addAddress(UserAddress $address)
    {
        $this->addresses[] = $address;
    }

    /**
     * @return mixed
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * @param mixed $panier
     */
    public function setPanier($panier)
    {
        $this->panier = $panier;
    }

    /**
     * @return mixed
     */
    public function getOrderings()
    {
        return $this->orderings;
    }

    /**
     * @param mixed $orderings
     */
    public function setOrderings($orderings)
    {
        $this->orderings = $orderings;
    }


    public function eraseCredentials()
    {
    }

    public function __toString()
    {
        return (string)$this->username;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }
}
