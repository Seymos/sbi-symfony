<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PanierRepository")
 */
class Panier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", name="date_creation")
     */
    private $date_creation;

    /**
     * @ORM\Column(type="string", name="livraison", nullable=true)
     */
    private $livraison;

    /**
     * @ORM\Column(type="string", name="facturation", nullable=true)
     */
    private $facturation;

    /**
     * @ORM\Column(type="string", name="status", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="float", name="total_ht", nullable=true)
     */
    private $total_ht;

    /**
     * @ORM\Column(type="float", name="total_tva", nullable=true)
     */
    private $total_tva;

    /**
     * @ORM\Column(type="float", name="total_ttc", nullable=true)
     */
    private $total_ttc;

    /**
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="panier")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Paniers have Many Products.
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", cascade={"persist"})
     * @ORM\JoinTable(name="paniers_products",
     *      joinColumns={@ORM\JoinColumn(name="panier_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private $products;

    /**
     * Panier constructor.
     */
    public function __construct()
    {
        $this->date_creation = new \DateTime('now');
        $this->products = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation): void
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return mixed
     */
    public function getLivraison()
    {
        return $this->livraison;
    }

    /**
     * @param string $livraison
     */
    public function setLivraison($livraison): void
    {
        $this->livraison = $livraison;
    }

    /**
     * @return mixed
     */
    public function getFacturation()
    {
        return $this->facturation;
    }

    /**
     * @param string $facturation
     */
    public function setFacturation($facturation): void
    {
        $this->facturation = $facturation;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTotalHt()
    {
        return $this->total_ht;
    }

    /**
     * @param mixed $total_ht
     */
    public function setTotalHt($total_ht): void
    {
        $this->total_ht = $total_ht;
    }

    /**
     * @return mixed
     */
    public function getTotalTva()
    {
        return $this->total_tva;
    }

    /**
     * @param mixed $total_tva
     */
    public function setTotalTva($total_tva): void
    {
        $this->total_tva = $total_tva;
    }

    /**
     * @return mixed
     */
    public function getTotalTtc()
    {
        return $this->total_ttc;
    }

    /**
     * @param mixed $total_ttc
     */
    public function setTotalTtc($total_ttc): void
    {
        $this->total_ttc = $total_ttc;
    }

}
