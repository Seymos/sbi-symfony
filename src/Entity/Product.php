<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="slug")
     */
    private $slug;

    /**
     * @ORM\Column(type="text", name="summary")
     */
    private $summary;

    /**
     * @ORM\Column(type="text", name="description")
     */
    private $description;

    /**
     * @ORM\Column(type="string", name="reference")
     */
    private $reference;

    /**
     * @ORM\Column(type="integer", name="quantity")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer", name="quantity_in_cart", nullable=true)
     */
    private $quantity_in_cart;

    /**
     * @ORM\Column(type="float", name="price_ht")
     */
    private $priceHt;

    /**
     * @ORM\Column(type="float", name="price_ttc", nullable=true)
     */
    private $priceTtc;

    /**
     * @ORM\Column(type="datetime", name="date_creation")
     */
    private $date_creation;

    /**
     * Many Features have One Product.
     * @ORM\Column(type="integer", name="category", nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="string", name="image")
     */
    private $image;

    /**
     * @ORM\Column(type="float", name="tva")
     */
    private $tva;

    public function __construct()
    {
        $this->date_creation = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     */
    public function setSummary($summary): void
    {
        $this->summary = $summary;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param integer $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantityInCart()
    {
        return $this->quantity_in_cart;
    }

    /**
     * @param mixed $quantity_in_cart
     */
    public function setQuantityInCart($quantity_in_cart): void
    {
        $this->quantity_in_cart = $quantity_in_cart;
    }

    /**
     * @return mixed
     */
    public function getPriceHt()
    {
        return $this->priceHt;
    }

    /**
     * @param float $priceHt
     */
    public function setPriceHt($priceHt): void
    {
        $this->priceHt = $priceHt;
    }

    /**
     * @return mixed
     */
    public function getPriceTtc()
    {
        return $this->priceTtc;
    }

    /**
     * @param float $priceTtc
     */
    public function setPriceTtc($priceTtc): void
    {
        $this->priceTtc = $priceTtc;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param \DateTime $date_creation
     */
    public function setDateCreation($date_creation): void
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param integer $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * @param float $tva
     */
    public function setTva($tva): void
    {
        $this->tva = $tva;
    }
}
