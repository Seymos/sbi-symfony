<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;

class Cart
{
    public function reinitCart(Request $request)
    {
        $session = $request->getSession();
        $session->clear();
        $panier = array();
        $panier['product_id'] = array();
        $panier['product_qty'] = array();
        $session->set('panier', $panier);
        return true;
    }

    public function create(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('panier')) {
            $panier = array();
            $panier['product_id'] = array();
            $panier['product_qty'] = array();
            $session->set('panier', $panier);
        }
        return true;
    }

    public function createCart(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('panier')) {
            $session->set('panier', array());
            $panier = $session->get('panier');
            $panier['product_id'] = array();
            $panier['product_qty'] = array();
        }
        return true;
    }

    public function addToCart(Request $request)
    {
        if ($this->createCart($request)) {
            $session = $request->getSession();
            $panier = $session->get('panier');
            // récupération id produit
            $product_id = intval($request->request->get('product_id'));
            // récupération qte produit
            $product_qte = intval($request->request->get('product_qty'));

            $position_produit = array_search($product_id, $panier['product_id']);
            if ($position_produit !== false) {
                $panier['product_qte'][$position_produit] += $product_qte;
            } else {
                array_push($panier['product_id'], $product_id);
                array_push($panier['product_qte'], $product_qte);
            }
            $session->set('panier', $panier);
            return true;
        } else {
            return false;
        }

    }


    public function removeFromCart(Request $request)
    {
        // récupérer la session
        $session = $request->getSession();
        // récupérer le panier
        $panier = $session->get('panier');
        // récupérer id produit
        $product_id = intval($request->get('product_id'));
        // boucle sur le panier
        // pour chaque item du panier
        // vérifier valeur de la clé
        foreach ($panier as $key => $value) {
            if ($product_id === $value) {
                unset($panier[$key]);
            }
        }
        $session->set('panier', $panier);
        $data = array(
            'type' => 'info',
            'message' => "Le produit a été supprimé de votre panier",
            'text' => "Supprimé"
        );
        return $data;
    }

    function modifieValeur($tab,$val,$remplace)
    {   $cle = array_search($val,$tab);
        if($cle!==false)
        {
            $tab[$cle]=$remplace;
            return $tab;
        }

    }
}