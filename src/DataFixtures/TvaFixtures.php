<?php

namespace App\DataFixtures;

use App\Entity\Tva;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TvaFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 5; $i++) {
            $tva = new Tva();
            $tva->setName("Tva$i");
            $tva->setValue($i/100);
            $manager->persist($tva);
        }
        $manager->flush();
    }
}
