<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 50; $i++) {
            $product = new Product();
            $slugify = new Slugify();
            $product->setName("Product$i");
            $product->setReference('product_'.uniqid());
            $product->setQuantity(mt_rand(50,100));
            $product->setPriceHt(mt_rand(10,100));
            $product->setTva(.2);
            $product->setSummary("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
            $product->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
            $product->setSlug($slugify->slugify("Product$i"));
            $product->setImage('https://dyw7ncnq1en5l.cloudfront.net/optim/produits/36/36047/samsung-galaxy-s8_8a0d10ed38f7af08__450_400.png');
            $manager->persist($product);
        }
        $manager->flush();
    }
}
