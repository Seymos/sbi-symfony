<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Ordering;
use App\Entity\Panier;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserAddress;
use App\Form\PanierType;
use App\Form\UserAddressType;
use App\Repository\CartRepository;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FunnelController extends Controller
{
    /**
     * @Route("/panier", name="cart")
     * @param Request $request
     * @return Response
     */
    public function panier(Request $request)
    {
        // récupération session
        $session = $request->getSession();
        if ($this->get('cart')->create($request) === false) {
            return $this->redirectToRoute('homepage');
        }
        $panier = $session->get('panier');
        $keys = array_keys($panier);
        $exist1 = array_search('product_id', $keys);
        $exist2 = array_search('product_qty', $keys);
        if ($exist1 === false && $exist2 === false) {
            // les clés n'existent pas
            return $this->redirectToRoute('homepage');
        }
        $em = $this->getDoctrine()->getManager();
        // récupération des quantités
        $qtes = $panier['product_qty'];
        // création tableau de produits
        $array_products = array();
        // boucle sur les id produits en panier
        foreach ($panier['product_id'] as $key => $value) {
            // récupération du produit correspondant en base
            $prod = $em->getRepository(Product::class)->find($value);
            // ajout du produit dans le tableau de produits
            array_push($array_products, $prod);
        }
        // boucle sur le tableau de produits
        foreach ($array_products as $key => $product) {
            // attribution quantité du produit depuis le panier
            $product->setQuantityInCart($qtes[$key]);
            // enregistrement en base
            $em->persist($product);
        }
        $em->flush();
        $session->set('panier_content', $array_products);
        return $this->render(
            'ecommerce/cart.html.twig',
            array(
                'panier' => $session->get('panier'),
                'products_panier' => $array_products
            )
        );
    }

    /**
     * @Route("/panier/validation", name="cart_validation")
     * @param Request $request
     * @return Response
     */
    public function validation_panier(Request $request)
    {
        // vérification utilisateur connecté
        if ($this->getUser() === null) {
            return $this->redirectToRoute('login');
        }
        // récupération utilisateur connecté
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        // récupération du panier de l'utilisateur courant
        $panier = $em->getRepository(Panier::class)->findOneByUser($user);
        if ($panier !== null) {
            return $this->redirectToRoute('cart_information');
        }
        // création panier avec date création immédiate
        $cart = new Panier();
        // set User
        $cart->setUser($user);
        // boucle sur produits en session
        $session_products = $request->getSession()->get('panier_content');
        foreach ($session_products as $key => $product) {
            // ajout des produits au panier
            $cart->getProducts()->add($product);
            //$cart->addProduct($product);
        }
        $em->persist($cart);
        $em->flush();
        return $this->redirectToRoute('cart_information');
    }

    /**
     * @Route("/panier/informations", name="cart_information")
     * @param Request $request
     * @return Response
     */
    public function userInformation(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        // récupération utilisateur courant
        $user = $this->getUser();
        // récupération du panier de l'utilisateur courant
        $panier = $em->getRepository(Panier::class)->findOneByUser($user);
        // récupération des produits du panier
        $products = $panier->getProducts();
        // création adresse pour utilisateur courant
        $user_address = new UserAddress($user);
        // création formulaire adresse
        $form = $this->createForm(UserAddressType::class, $user_address);
        // création formulaire panier (adresses)
        $panier_form = $this->createForm(PanierType::class, $panier);

        $panier_form->handleRequest($request);
        $form->handleRequest($request);

        // formulaire ajout adresse
        if ($form->isSubmitted() && $form->isValid()) {
            $street = $user_address->getStreet();
            $postal = $user_address->getCodePostal();
            $city = $user_address->getCity();
            $country = $user_address->getCountry();
            $user_address->setFull($street." ".$postal." ".$city.", ".$country);
            // enregistrement adresse
            $em->persist($user_address);
            $em->flush();
            return $this->redirectToRoute('cart_information');
        }
        // formulaire panier
        if ($panier_form->isSubmitted() && $panier_form->isValid()) {
            $facturation = $panier->getFacturation();
            $livraison = $panier->getLivraison();
            $panier->setFacturation($facturation->getFull());
            $panier->setLivraison($livraison->getFull());
            // enregistrement panier
            $em->persist($panier);
            $em->flush();
            return $this->redirectToRoute('cart_information_validation');
        }
        // récupération des adresses de l'utilisateur courant
        $addresses = $em->getRepository(UserAddress::class)->findBy(
            array(
                'user' => $user
            )
        );
        return $this->render(
            'user/user_info.html.twig',
            array(
                'form' => $form->createView(),
                'addresses' => $addresses,
                'panier_form' => $panier_form->createView(),
                'products' => $products
            )
        );
    }

    /**
     * @Route("/panier/informations/validation", name="cart_information_validation")
     * @param Request $request
     * @return Response
     */
    public function validateUserInformation(Request $request)
    {
        // récupération session
        $session = $request->getSession();
        // récupération utilisateur courant
        $user = $this->getUser();
        // récupération panier utilisateur courant
        $panier = $this->getDoctrine()->getRepository(Panier::class)->findOneByUser($user);
        return $this->render(
            'user/validate_user_info.html.twig',
            array(
                'panier' => $panier
            )
        );
    }

    /**
     * @Route("/reglement-commande", name="pay")
     * @param Request $request
     */
    public function pay(Request $request)
    {
        $session = $request->getSession();
        // récupération utilisateur courant
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        // récupération panier utilisateur courant
        $panier = $this->getDoctrine()->getRepository(Panier::class)->findOneByUser($user);
        $ordering = array(
            'total_ht' => 0,
            'total_ttc' => 0,
            'total_tva' => 0
        );
        // boucle sur produits panier
        foreach ($panier->getProducts() as $key => $product) {
            // qté sélectionnée
            $qte_cart = $product->getQuantityInCart();
            // qté stock
            $qte_av = $product->getQuantity();
            // tarif/unité HT
            $price_u_ht = $product->getPriceHt();
            // TVA
            $tva = $product->getTva();
            // calcul total_ht
            $total_ht = $price_u_ht * $qte_cart;
            // total tva
            $total_tva = ($tva * $price_u_ht) * $qte_cart;
            // calcul tarif TTC
            $ttc = ($price_u_ht*$tva) + $price_u_ht;
            // calcul total TTC
            $total_ttc = $total_tva + $total_ht;

            $ordering['total_ht'] += $total_ht;
            // total_ttc
            $ordering['total_ttc'] += $total_ttc;
            $ordering['total_tva'] += $total_tva;
        }
        $session->set('ordering', $ordering);
        $panier->setTotalHt($ordering['total_ht']);
        $panier->setTotalTva($ordering['total_tva']);
        $panier->setTotalTtc($ordering['total_ttc']);
        $panier->setStatus("En attente de paiement");
        $em->persist($panier);
        $em->flush();
        return $this->render(
            'user/payment.html.twig',
            array(
                'order' => $ordering,
                'panier' => $panier
            )
        );
    }

    /**
     * @Route("/reglement-recu/{id}", name="payment_accepted")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function payer(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        // récupération panier en attente de paiement
        $panier = $em->getRepository(Panier::class)->findOneByUser($user);
        // si paiement reçu et validé
        // créer commande avec status
        $ordering = new Ordering();
        $ordering->setStatus("Paiement reçu");
        $ordering->setDateCreation($panier->getDateCreation());
        $ordering->setDateCharged(new \DateTime('now'));
        $ordering->setLivraison($panier->getLivraison());
        $ordering->setFacturation($panier->getFacturation());
        $ordering->setTotalTva($panier->getTotalTva());
        $ordering->setTotalTtc($panier->getTotalTtc());
        $ordering->setTotalHt($panier->getTotalHt());
        $ordering->setUser($user);
        $ordering->setProducts($panier->getProducts());
        // supprimer le panier en base
        $em->remove($panier);
        $em->persist($ordering);
        $em->flush();
        // envoi email confirmation de commande
        // réinitialiser panier session
        $this->get('cart')->reinitCart($request);
        return $this->render(
            'user/payment_accepted.html.twig',
            array(
                'ordering' => $ordering
            )
        );
    }

    /**
     * @Route("/clear-session", name="session_clear")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function clearSession(Request $request)
    {
        $session = $request->getSession();
        $session->clear();
        return $this->redirectToRoute('homepage');
    }
}
