<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PublicController extends Controller
{
    /**
     * @Route("/mentions-legales", name="mentions_legales")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mentions()
    {
        return $this->render(
            'public/mentions.html.twig'
        );
    }

    /**
     * @Route("/conditions-generales-de-vente", name="cgv")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cgv()
    {
        return $this->render(
            'public/cgv.html.twig'
        );
    }
}
