<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\EmailFormType;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastusername = $authenticationUtils->getLastUsername();
        return $this->render(
            'security/login.html.twig',
            array(
                'error' => $error,
                'last_username' => $lastusername
            )
        );
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {}

    /**
     * @Route("/inscription", name="register")
     * @param Request $request
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $encoded = $encoder->encodePassword($user, $data->getPassword());
            $user->setPassword($encoded);
            $user->setUsername($data->getEmail());
            $em->persist($user);
            $em->flush();
            // envoi email de confirmation
            return $this->redirectToRoute('login');
        }
        return $this->render(
            'security/register.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/account/change-password", name="change_password")
     * @param Request $request
     * @return Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $encoded = $encoder->encodePassword($user, $data->getPassword());
            $user->setPassword($encoded);
            $em->persist($user);
            $em->flush();
            // envoi email de confirmation
            return $this->redirectToRoute('account');
        }
        return $this->render(
            'security/change_password.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/ask-reset-password-link", name="ask_reset_password_link")
     * @param Request $request
     * @return Response
     */
    public function askResetPasswordLink(Request $request)
    {
        $form = $this->createForm(EmailFormType::class);
        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->getData()->getEmail();
            $user = $em->getRepository(User::class)->findOneByEmail($email);
            if ($user === null) {
                return $this->redirectToRoute('homepage');
            } else {
                $user->setToken();
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('homepage');
            }
        }
        return $this->render(
            'security/ask_reset_link.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneByToken($token);
        if ($user === null) {
            return $this->redirectToRoute('homepage');
        } else {
            $form = $this->createForm(ChangePasswordType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $encoded = $encoder->encodePassword($user, $data->getPassword());
                $user->setPassword($encoded);
                $user->resetToken();
                $em->persist($user);
                $em->flush();
                // envoi email de confirmation
                return $this->redirectToRoute('login');
            }
            return $this->render(
                'security/change_password.html.twig',
                array(
                    'form' => $form->createView()
                )
            );
        }
    }
}
