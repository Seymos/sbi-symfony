<?php

namespace App\Controller;

use App\Entity\Ordering;
use App\Entity\UserAddress;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccountController extends Controller
{
    /**
     * @Route("/account", name="account")
     */
    public function index()
    {
        $user = $this->getUser();
        $addresses = $this->getDoctrine()->getRepository(UserAddress::class)->findBy(
            array(
                'user' => $user
            )
        );
        return $this->render(
            'account/index.html.twig',
            array(
                'user' => $user,
                'addresses' => $addresses
            )
        );
    }

    /**
     * @Route("/account/addresses", name="account_addresses")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addresses()
    {
        $user = $this->getUser();
        $addresses = $this->getDoctrine()->getRepository(UserAddress::class)->findBy(
            array(
                'user' => $user
            )
        );
        return $this->render(
            'account/addresses.html.twig',
            array(
                'addresses' => $addresses
            )
        );
    }

    /**
     * @Route("/account/orderings", name="account_orderings")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderings()
    {
        $user = $this->getUser();
        $orderings = $this->getDoctrine()->getRepository(Ordering::class)->findBy(
            array(
                'user' => $user
            )
        );
        $this_mount = $orderings;
        return $this->render(
            'account/orderings.html.twig',
            array(
                'orderings' => $orderings,
                'this_mounth' => $this_mount
            )
        );
    }
}
