<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PanierController extends Controller
{
    public function createPanier(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('panier')) {
            $panier = array();
            $panier['product_id'] = array();
            $panier['product_qty'] = array();
            $session->set('panier', $panier);
        }
        return true;
    }
    /**
     * @Route("add-to-cart", name="add_cart")
     * @param Request $request
     * @return Response
     */
    public function addToCart(Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            if ($this->createPanier($request)) {
                $panier = $session->get('panier');
                // récupération id produit
                $product_id = intval($request->request->get('product_id'));
                // récupération qte produit
                $product_qte = intval($request->request->get('product_qty'));
                $position_produit = array_search($product_id, $panier['product_id']);
                if ($position_produit !== false) {
                    $panier['product_qty'][$position_produit] = $product_qte;
                } else {
                    array_push($panier['product_id'], $product_id);
                    array_push($panier['product_qty'], $product_qte);
                }
                $session->set('panier', $panier);
                return $this->redirectToRoute('cart');
            } else {
                return $this->redirectToRoute('homepage');
            }
        }
    }

    /**
     * @Route("/remove-from-panier", name="remove_product_from_cart")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeFromCart(Request $request)
    {
        if ($request->isMethod('POST')) {
            $session = $request->getSession();
            $panier = $session->get('panier');
            $product_id = intval($request->request->get('product__id'));
            $em = $this->getDoctrine();
            $product = $em->getRepository(Product::class)->find($product_id);
            $product->setQuantityInCart(null);
            $manager = $em->getManager();
            $manager->persist($product);
            $manager->flush();
            $temp = array();
            $temp['product_id'] = array();
            $temp['product_qty'] = array();
            for ($i = 0; $i < count($panier['product_id']); $i++) {
                if ($panier['product_id'][$i] !== $product_id) {
                    array_push($temp['product_id'], $panier['product_id'][$i]);
                    array_push($temp['product_qty'], $panier['product_qty'][$i]);
                }
            }
            $session->set('panier', $temp);
            unset($temp);
            return $this->redirectToRoute('cart');
        } else {
            return $this->redirectToRoute('cart');
        }
    }

    /**
     * @Route("/edit-qte", name="edit_qte")
     * @param Request $request
     * @return Response
     */
    public function editQte(Request $request)
    {
        if ($request->isMethod('POST')) {
            $session = $request->getSession();
            $panier = $session->get('panier');
            // récupération id produit
            $product_id = intval($request->request->get('product___id'));
            // récupération qte produit
            $product_qte = intval($request->request->get('product___qte'));
            $em = $this->getDoctrine();
            $product = $em->getRepository(Product::class)->find($product_id);
            $product->setQuantityInCart($product_qte);
            $manager = $em->getManager();
            $manager->persist($product);
            $manager->flush();
            if ($product_qte > 0) {
                $position_produit = array_search($product_id, $panier['product_id']);
                if ($position_produit !== false) {
                    $panier['product_qty'][$position_produit] = $product_qte;
                }
            }
            $session->set('panier', $panier);
            return $this->redirectToRoute('cart');
        } else {
            return $this->redirectToRoute('cart');
        }
    }
}
