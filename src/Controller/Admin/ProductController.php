<?php

namespace App\Controller\Admin;


use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/admin/products", name="admin_product_list")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request)
    {
        $em = $this->getDoctrine();
        $products = $em->getRepository(Product::class)->all();
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($form->getData());
            die('ici create product');
        }
        return $this->render(
            'admin/product/list.html.twig',
            array(
                'products' => $products,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/admin/products/show/{product}", name="admin_product_show")
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Product $product)
    {
        return $this->render(
            'admin/product/show.html.twig',
            array(
                'product' => $product
            )
        );
    }

    /**
     * @Route("/admin/products/edit/{product}", name="admin_product_edit")
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function edit(Product $product, Request $request)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($form->getData());
            die('ici edit product');
        }
        return $this->render(
            'admin/category/edit.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/admin/products/delete/{product}", name="admin_product_delete")
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();
        return $this->redirectToRoute('admin_product_list');
    }
}
