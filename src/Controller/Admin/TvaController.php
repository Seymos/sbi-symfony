<?php

namespace App\Controller\Admin;

use App\Entity\Tva;
use App\Form\TvaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TvaController extends Controller
{
    /**
     * @Route("/admin/tva", name="admin_tva_list")
     * @param Request $request
     * @return Response
     */
    public function list(Request $request)
    {
        $tva = new Tva();
        $form = $this->createForm(TvaType::class, $tva);
        $em = $this->getDoctrine();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tva->setValue($form->getData()->getValue()/100);
            $manager = $em->getManager();
            $manager->persist($tva);
            $manager->flush();
            return $this->redirectToRoute('admin_tva_list');
        }
        $tvas = $em->getRepository(Tva::class)->findAll();
        return $this->render(
            'admin/tva/list.html.twig',
            array(
                'form' => $form->createView(),
                'tvas' => $tvas
            )
        );
    }

    /**
     * @Route("/admin/tva/edit/{tva}", name="admin_tva_edit")
     * @param Tva $tva
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Tva $tva, Request $request)
    {
        $form = $this->createForm(TvaType::class, $tva);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tva);
            $em->flush();
            return $this->redirectToRoute('admin_tva_list');
        }
        return $this->render(
            'admin/tva/edit.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/admin/tva/delete/{tva}", name="admin_tva_delete")
     * @param Tva $tva
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Tva $tva) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tva);
        $em->flush();
        return $this->redirectToRoute('admin_tva_list');
    }
}
