<?php

namespace App\Controller\Admin;

use App\Form\CategoryType;
use Cocur\Slugify\Slugify;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route("/admin/categories", name="admin_category_list")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request)
    {
        $em = $this->getDoctrine();
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $em->getManager();
            $slugify = new Slugify();
            $category->setSlug($slugify->slugify($category->getName()));
            $manager->persist($category);
            $manager->flush();
            return $this->redirectToRoute('admin_category_list');
        }
        $categories = $em->getRepository(Category::class)->findAll();
        return $this->render(
            'admin/category/list.html.twig',
            array(
                'categories' => $categories,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/admin/category/edit/{category}", name="admin_category_edit")
     * @param Category $category
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Category $category, Request $request)
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('admin_category_list');
        }
        return $this->render(
            'admin/category/edit.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/admin/category/delete/{category}", name="admin_category_delete")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
        return $this->redirectToRoute('admin_category_list');
    }
}
