<?php

namespace App\Controller\Admin;


use App\Entity\Panier;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CartController extends Controller
{
    /**
     * @Route("/admin/orders", name="admin_cart_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {
        $carts = $this->getDoctrine()->getRepository(Panier::class)->findAll();
        return $this->render(
            'admin/cart/list.html.twig',
            array(
                'carts' => $carts
            )
        );
    }
}