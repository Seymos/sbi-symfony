<?php

namespace App\Controller\Admin;

use App\Entity\Ordering;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class OrderingController extends Controller
{
    /**
     * @Route("/admin/commandes", name="admin_order_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {
        $carts = $this->getDoctrine()->getRepository(Ordering::class)->findAll();
        return $this->render(
            'admin/ordering/list.html.twig',
            array(
                'carts' => $carts
            )
        );
    }

    /**
     * @Route("/admin/commandes/envoi", name="admin_order_send")
     */
    public function sendDailyOrderings()
    {
        $new_date = new \DateTime('now');
        $today = $new_date->format('d-m-Y');
        $orderings = $this->getDoctrine()->getRepository(Ordering::class)->findBy(
            array(
                'date_charged' => $today
            )
        );
        dump($orderings);
        die();
    }
}