<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * @Route("/admin/users", name="admin_user_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render(
            'admin/user/list.html.twig',
            array(
                'users' => $users
            )
        );
    }
}
