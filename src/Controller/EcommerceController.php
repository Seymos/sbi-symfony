<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Tva;
use App\Entity\User;
use App\Form\ProductType;
use App\Services\Cart;
use Cocur\Slugify\Slugify;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EcommerceController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->get('cart')->create($request);
        $session = $request->getSession();
        $panier = $session->get('panier');
        $em = $this->getDoctrine();
        $categories = $em->getRepository(Category::class)->findAll();
        $products = $em->getRepository(Product::class)->all();
        return $this->render(
            'ecommerce/index.html.twig',
            array(
                'categories' => $categories,
                'products' => $products,
                'panier' => $panier
            )
        );
    }

    /**
     * @Route("/product/{slug}", name="product")
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function product(string $slug, Request $request)
    {
        $em = $this->getDoctrine();
        $product = $em->getRepository(Product::class)->findOneBy(
            array(
                'slug' => $slug
            )
        );
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $em->getManager();
            $category = $em->getRepository(Category::class)->find($form->getData()->getCategory()->getId());
            $category->addProduct($product);
            $manager->persist($category);
            $manager->persist($product);
            $manager->flush();
            return $this->redirectToRoute('product', array('slug' => $slug));
        }
        return $this->render(
            'ecommerce/product.html.twig',
            array(
                'product' => $product,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/category/{slug}/", name="category")
     * @param string $slug
     * @return Response
     */
    public function category($slug)
    {
        // a optimiser
        $em = $this->getDoctrine();
        $category = $em->getRepository(Category::class)->findOneBy(
            array(
                'slug' => $slug
            )
        );
        $products = $em->getRepository(Product::class)->findBy(
            array(
                'category' => $category
            )
        );
        $categories = $em->getRepository(Category::class)->findAll();
        return $this->render(
            'ecommerce/category.html.twig',
            array(
                'products' => $products,
                'category' => $category,
                'categories' => $categories
            )
        );
    }

    /**
     * @Route("/panier-test", name="panier_test")"
     * @return Response
     */
    public function panier_test()
    {
        $em = $this->getDoctrine();
        $paniers = $em->getRepository(\App\Entity\Cart::class)->findAll();
        return $this->render(
            'ecommerce/panier_test.html.twig',
            array(
                'paniers' => $paniers
            )
        );
    }
}
