<?php

namespace App\Form;

use App\Entity\Panier;
use App\Entity\UserAddress;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PanierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'livraison',
                EntityType::class,
                array(
                    'class' => UserAddress::class,
                    'choice_label' => 'full',
                    'choice_value' => "id"
                )
            )
            ->add(
                'facturation',
                EntityType::class,
                array(
                    'class' => UserAddress::class,
                    'choice_label' => 'full',
                    'choice_value' => "id"
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Panier::class,
        ]);
    }
}
