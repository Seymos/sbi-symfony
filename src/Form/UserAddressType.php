<?php

namespace App\Form;

use App\Entity\UserAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'type',
                ChoiceType::class,
                array(
                    'choices' => array(
                        "Livraison" => 'livraison',
                        "Facturation" => 'facturation'
                    )
                )
            )
            ->add(
                'street',
                TextType::class
            )
            ->add(
                'complement',
                TextType::class,
                array(
                    'required' => false
                )
            )
            ->add(
                'codePostal',
                TextType::class
            )
            ->add(
                'city',
                TextType::class
            )
            ->add(
                'country',
                TextType::class
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => UserAddress::class,
        ]);
    }
}
